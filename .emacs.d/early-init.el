;;; early-init.el --- Emacs 27 early initialization -*- lexical-binding: t; -*-

;;; Commentary:
;; Emacs 27 introduced early-init.el, which runs before package and UI
;; initialization. This provides an opportunity to change some of the
;; defaults that impact startup speed.

;;; Code:

;; Defer garbage collection. We'll reset this to a reasonable value
;; after finishing the startup process.
(setq gc-cons-threshold most-positive-fixnum)

;; We handle package initialization ourselves, so stop Emacs from
;; doing it automatically.
(setq package-enable-at-startup nil)

;; We also don't need to check for file name handlers until after
;; startup.
(defvar original-file-name-handler-alist file-name-handler-alist)
(setq file-name-handler-alist nil)

;; Turn off GUI options that we don't need before any frames are created.
(push '(menu-bar-lines . 0) default-frame-alist)
(push '(tool-bar-lines . 0) default-frame-alist)
(push '(vertical-scroll-bars . nil) default-frame-alist)
(push '(ns-transparent-titlebar . t) default-frame-alist)

;; Prevent expensive resizing, and set the default font.
(setq frame-inhibit-implied-resize t)
(set-face-attribute 'default nil :family "IBM Plex Mono" :height 140)

;; Ignore X resources; we're not using them.
(advice-add #'x-apply-session-resources :override #'ignore)

;;; early-init.el ends here
