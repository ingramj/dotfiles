;;; init-text.el --- Text editing, including markup -*- lexical-binding: t; -*-

;;; Commentary:
;; Basic text editing, and modes for various markup languages. Right
;; now that's just markdown and org.

;;; Code:

;; Text editing defaults.
(setq-default fill-column 72)
(setq-default indent-tabs-mode nil)
(show-paren-mode)
(visual-line-mode)

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode)
  :mode ("\\.md\\'" . markdown-mode)
  :hook (markdown-mode
         . (lambda ()
             (setq markdown-command "/opt/local/bin/multimarkdown")
             (visual-line-mode))))

(use-package org
  :ensure t
  :mode (("\\.org\\'" . org-mode)))

(provide 'init-text)

;;; init-text.el
