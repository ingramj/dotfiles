;;; interactive.el --- Custom interactive commands -*- lexical-binding: t; -*-

;;; Commentary:
;; Interactive function definitions and key bindings.

;;; Code:

(defun switch-to-scratch-and-back ()
  "Toggles between the *scratch* buffer and the current buffer.
Creates a new *scratch* buffer if one doesn't already exist."
  (interactive)
  (let ((scratch-buffer (get-buffer-create "*scratch*")))
    (if (equal (current-buffer) scratch-buffer)
        (switch-to-buffer (other-buffer))
      (progn
        (switch-to-buffer scratch-buffer)
        (lisp-interaction-mode)
        (or (> (buffer-size) 0)
            (insert (substitute-command-keys initial-scratch-message)))))))

(bind-key "C-x y" #'switch-to-scratch-and-back)

(provide 'interactive)

;;; interactive.el ends here
