;;; init-ui.el --- UI configuration and theme -*- lexical-binding: t; -*-

;;; Commentary:
;; Some UI configuration is done in early-init.el, to make startup
;; times faster. The rest of it is done here. This file should be
;; loaded after init-package.el, in case a third-party theme is used.

;;; Code:

;; Turn of a few more UI pieces that we don't care about.
(setq inhibit-startup-screen t)
(setq ns-use-proxy-icon nil)
(setq frame-title-format nil)

;; Show line and column numbers in the mode-line only.
(line-number-mode t)
(column-number-mode t)
(display-line-numbers-mode)

;; Color theme.
(load-theme 'misterioso t)

(provide 'init-ui)

;;; init-ui.el ends here
