;;; init-start.el --- Miscellaneous startup actions -*- lexical-binding: t; -*-

;;; Commentary:
;; Things that should be done at startup, but don't require any extra
;; packages to be installed.

;;; Code:

;; The GC threshold and file name handlers alist were overridden in
;; early-init.el. Reset them here, but use a more reasonable GC size.
(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold 16777216) ; 16M instead of 800k.
            (setq file-name-handler-alist original-file-name-handler-alist)
            (makunbound 'original-file-name-handler-alist)))

;; ;; When Emacs is launched with Cmd-Space, default-directory is set to
;; "/", which isn't especially useful. Set it to "~/" instead. If for
;; some reason Emacs is launched from a terminal where "/" is the cwd,
;; this may be confusing. But that's not a thing that I do often.
(and (equal default-directory "/")
     (setq default-directory "~/"))

;; Kill the bell.
(setq-default ring-bell-function 'ignore)

;; Don't litter.
(setq backup-directory-alist
      '(("." . "~/.emacs.d/backup")))

(provide 'init-start)

;;; init-start.el ends here
