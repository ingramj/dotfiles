;;; init-package.el --- Set up the package manager -*- lexical-binding: t; -*-

;;; Commentary:
;; Initialize package.el and make sure that use-package is installed.
;; This should be loaded early by init.el, before any libraries that
;; require packages.

;;; Code:

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))
(require 'bind-key)

(provide 'init-package)

;;; init-package.el ends here
