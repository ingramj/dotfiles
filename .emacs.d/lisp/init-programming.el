;;; init-programming.el --- Working with source code -*- lexical-binding: t; -*-

;;; Commentary:
;; Modes and configuration settings for programming. Right now this is
;; just for C. If this starts getting too long because of additional
;; languages being added, it could be split up.

;;; Code:

(use-package smart-tabs-mode
  :ensure t)

(use-package cc-mode
  :ensure nil
  :hook (c-mode
         . (lambda ()
             (c-set-style "k&r")
             (setq c-basic-offset 8
                   tab-width 8
                   indent-tabs-mode t
                   require-final-newline t
                   c-auto-align-backslashes nil)
             (smart-tabs-insinuate 'c)
             (local-set-key (kbd "RET") 'c-context-line-break)
             (add-to-list 'write-file-functions 'delete-trailing-whitespace))))

(provide 'init-programming)

;;; init-programming.el ends here
