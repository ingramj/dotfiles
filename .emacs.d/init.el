;;; init.el --- Emacs startup file -*- lexical-binding: t; -*-

;;; Commentary:
;; This mostly just loads other files in the 'lisp/' directory.

;;; Code:

(add-to-list 'load-path (expand-file-name "lisp/" user-emacs-directory))
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))

(require 'init-start)
(require 'init-package)
(require 'init-ui)
(require 'init-text)
(require 'init-programming)
(require 'interactive)

(when (file-exists-p custom-file)
  (load-file custom-file))

;;; init.el ends here
