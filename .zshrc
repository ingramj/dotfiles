# Environment (Interactive Only) --------------------------------------
export EDITOR=mg
export VISUAL=emacs

# History --------------------------------------------------------------
HISTSIZE=500                   # Load 500 lines from history file
SAVEHIST=500                   # Save 500 lines to history file
HISTFILE=~/.history            # Save history in ~/.history
setopt hist_verify             # Show history substitution before executing
setopt share_history           # Add and import lines incrementally
setopt hist_ignore_all_dups    # Remove old duplicate commands
setopt hist_ignore_space       # Do not save lines beginning with a space

# Prompt ---------------------------------------------------------------
autoload -U colors && colors
PS1="%{${fg[red]}%}%(0?..(%?%))%f%{${fg[cyan]}%}%#%f "
RPS1="%{${fg[yellow]}%}%~%{${fg[default]}%}"

# Key Bindings ---------------------------------------------------------
if [[ -f ~/.zkbd/$TERM-$VENDOR-$OSTYPE ]]; then
    source ~/.zkbd/$TERM-$VENDOR-$OSTYPE
else
    print Unable to find zkbd file. Run 'autoload zkbd && zkbd' to create one.
fi

[[ -n ${key[Up]} ]]        && bindkey "${key[Up]}"        up-line-or-search
[[ -n ${key[Down]} ]]      && bindkey "${key[Down]}"      down-line-or-search
[[ -n ${key[Left]} ]]      && bindkey "${key[Left]}"      backward-char
[[ -n ${key[Right]} ]]     && bindkey "${key[Right]}"     forward-char
[[ -n ${key[PageUp]} ]]    && bindkey "${key[PageUp]}"    up-line-or-history
[[ -n ${key[PageDown]} ]]  && bindkey "${key[PageDown]}"  down-line-or-history
[[ -n ${key[Insert]} ]]    && bindkey "${key[Insert]}"    overwrite-mode
[[ -n ${key[Home]} ]]      && bindkey "${key[Home]}"      beginning-of-line
[[ -n ${key[End]} ]]       && bindkey "${key[End]}"       end-of-line
[[ -n ${key[Delete]} ]]    && bindkey "${key[Delete]}"    delete-char
[[ -n ${key[Backspace]} ]] && bindkey "${key[Backspace]}" backward-delete-char

# Completions ----------------------------------------------------------
autoload -U compinit && compinit
export LS_COLORS="di=34:ln=35:so=32:pi=33:ex=31:bd=34;46:cd=34;43:su=30;41:sg=30;46:tw=30;42:ow=30;43"
zstyle ':completion:*:default' list-colors ${(@s.:.)LS_COLORS}

# Aliases --------------------------------------------------------------
alias ls='ls -G -F'
alias cfg='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
